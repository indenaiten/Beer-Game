//IMPORTS
import React from "react";

import "./Proof.css";

import proofsData from "./data/proofs.json";

import RandomService from "../../services/random/RandomService";

import Game from "./proofs/Game/Game";
import Duel from "./proofs/Duel/Duel";
import Confession from "./proofs/Confession/Confession";
import Order from "./proofs/Order/Order";
import Rule from "./proofs/Rule/Rule";
import Trivial from "./proofs/Trivial/Trivial";
import Never from "./proofs/Never/Never";
import Named from "./proofs/Named/Named";





/** Proof Component. */
export default class Proof extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random Service. */
    this.randomService = new RandomService();

    /** Proofs data. */
    this.proofs = proofsData.proofs;
  }


  /** Gets a random Proof Component. */
  getProof(){
    let result = null;
    let index = this.randomService.get( 0, this.proofs.length );
    let proof = this.proofs[ index ];
    
    //Select Component
    switch( proof.id.toLowerCase() ){
        case "game": result = <Game proof={proof}/>; break;
        case "duel": result = <Duel proof={proof}/>; break;
        case "confession": result = <Confession proof={proof}/>; break;
        case "order": result = <Order proof={proof}/>; break;
        case "rule": result = <Rule proof={proof}/>; break;
        case "trivial": result = <Trivial proof={proof}/>; break;
        case "never": result = <Never proof={proof}/>; break;
        case "named": result = <Named proof={proof}/>; break;
        default: break;
    }

    return result;
  }


  /** Render. */
  render(){    
    return (
      <main className="proof">
        {this.getProof()}
      </main>
    );
  }

}//End Component
