//IMPORTS
import React from "react";

import "./Game.css";

import RandomService from "../../../../services/random/RandomService";

import gamesData from "./data/games.json";





/** Game Component. */
export default class Game extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Games data. */
    this.games = gamesData.games;
  }


  /** Gets a random Game. */
  getGame(){
    let index = this.randomService.get( 0, this.games.length );
    return this.games[ index ];
  }


  /** Render. */
  render(){
    let game = this.getGame();

    return (
      <div className="wrapper game">
        <h1>{this.props.proof.name}</h1>
        <h2>{game.name}</h2>
      </div>
    );
  }

}//End Component
