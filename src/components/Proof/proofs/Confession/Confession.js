//IMPORTS
import React from "react";

import "./Confession.css";

import RandomService from "../../../../services/random/RandomService";

import confessionsData from "./data/confessions.json";





/** Confession Component. */
export default class Confession extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Confessions data. */
    this.confessions = confessionsData.confessions;
  }


  /** Gets a random confession. */
  getConfession(){
    let index = this.randomService.get( 0, this.confessions.length );
    return this.confessions[ index ];
  }


  /** Render. */
  render(){
    let confession = this.getConfession();

    return (
      <div className="wrapper confession">
        <h1>{this.props.proof.name}</h1>
        <h2>{confession.name}</h2>
      </div>
    );
  }

}//End Component
