//IMPORTS
import React from "react";

import "./Duel.css";

import RandomService from "../../../../services/random/RandomService";

import duelsData from "./data/duels.json";





/** Duel Component. */
export default class Duel extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Duels data. */
    this.duels = duelsData.duels;
  }


  /** Gets a random duel. */
  getDuel(){
    let index = this.randomService.get( 0, this.duels.length );
    return this.duels[ index ];
  }


  /** Render. */
  render(){
    let duel = this.getDuel();

    return (
      <div className="wrapper duel">
        <h1>{this.props.proof.name}</h1>
        <h2>{duel.name}</h2>
      </div>
    );
  }

}//End Component
