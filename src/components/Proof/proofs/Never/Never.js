//IMPORTS
import React from "react";

import "./Never.css";

import RandomService from "../../../../services/random/RandomService";

import neversData from "./data/nevers.json";





/** Never Component. */
export default class Never extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Nevers data. */
    this.nevers = neversData.nevers;
  }


  /** Gets a random never. */
  getNever(){
    let index = this.randomService.get( 0, this.nevers.length );
    return this.nevers[ index ];
  }


  /** Render. */
  render(){
    let never = this.getNever();

    return (
      <div className="wrapper never">
        <h1>{this.props.proof.name}</h1>
        <h2>{never.name}</h2>
      </div>
    );
  }

}//End Component
