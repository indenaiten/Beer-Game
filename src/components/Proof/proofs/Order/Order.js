//IMPORTS
import React from "react";

import "./Order.css";

import RandomService from "../../../../services/random/RandomService";

import ordersData from "./data/orders.json";





/** Order Component. */
export default class Order extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Orders data. */
    this.orders = ordersData.orders;
  }


  /** Gets a random order. */
  getOrder(){
    let index = this.randomService.get( 0, this.orders.length );
    return this.orders[ index ];
  }


  /** Render. */
  render(){
    let order = this.getOrder();

    return (
      <div className="wrapper order">
        <h1>{this.props.proof.name}</h1>
        <h2>{order.name}</h2>
      </div>
    );
  }

}//End Component
