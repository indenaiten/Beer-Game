//IMPORTS
import React from "react";

import "./Trivial.css";

import RandomService from "../../../../services/random/RandomService";

import trivialsData from "./data/trivials.json";





/** Trivial Component. */
export default class Trivial extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Trivials data. */
    this.trivials = trivialsData.trivials;
  }


  /** Gets a random trivial. */
  getTrivial(){
    let index = this.randomService.get( 0, this.trivials.length );
    return this.trivials[ index ];
  }


  /** Render. */
  render(){
    let trivial = this.getTrivial();

    return (
      <div className="wrapper trivial">
        <h1>{this.props.proof.name}</h1>
        <h2>{trivial.question}</h2>
      </div>
    );
  }

}//End Component
