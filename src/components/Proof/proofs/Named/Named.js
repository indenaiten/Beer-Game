//IMPORTS
import React from "react";

import "./Named.css";

import RandomService from "../../../../services/random/RandomService";

import namedsData from "./data/nameds.json";





/** Named Component. */
export default class Named extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Nameds data. */
    this.nameds = namedsData.nameds;
  }


  /** Gets a random named. */
  getNamed(){
    let index = this.randomService.get( 0, this.nameds.length );
    return this.nameds[ index ];
  }


  /** Render. */
  render(){
    let named = this.getNamed();

    return (
      <div className="wrapper named">
        <h1>{this.props.proof.name}</h1>
        <h2>{named.name}</h2>
      </div>
    );
  }

}//End Component
