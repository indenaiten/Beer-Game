//IMPORTS
import React from "react";

import "./Rule.css";

import RandomService from "../../../../services/random/RandomService";

import rulesData from "./data/rules.json";





/** Rule Component. */
export default class Rule extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    /** Random service. */
    this.randomService = new RandomService();

    /** Rules data. */
    this.rules = rulesData.rules;
  }


  /** Gets a random rule. */
  getRule(){
    let index = this.randomService.get( 0, this.rules.length );
    return this.rules[ index ];
  }


  /** Render. */
  render(){
    let rule = this.getRule();

    return (
      <div className="wrapper rule">
        <h1>{this.props.proof.name}</h1>
        <h2>{rule.name}</h2>
      </div>
    );
  }

}//End Component
