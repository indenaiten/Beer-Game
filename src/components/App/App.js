//IMPORTS
import React from "react";

import "./App.css";

import Proof from "../Proof/Proof";





/** App Component. */
export default class App extends React.Component{

  /** Constructor. */
  constructor( props ){
    super( props );

    //Initialize State
    this.state = { proof: null }
  }


  /** Handle click. */
  handleClick(){
    //Sets in state a Proof Component
    this.setState({ proof: <Proof/> })
  }


  /** Render. */
  render(){
    return (
      <div id="wrapper-main" className="wrapper app">
          {this.state.proof}

          <button onClick={this.handleClick.bind( this )}>Throw</button>
      </div>
    );
  }

}//Ends Component
