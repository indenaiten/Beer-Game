/** Random Service. */
export default class RandomService{

    /** Constructor */
    constructor(){}

    
    /** Gets Random. */
    get( min, max ){
        return Math.floor( Math.random() * max ) + min;
    }

}//End Service